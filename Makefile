UILD_DIR=build
APP=app-go-example
GOTAGS?=musl

gomod:
	go mod tidy && go mod vendor
.PHONY: gomod

linter:
	golangci-lint run
.PHONY: linter