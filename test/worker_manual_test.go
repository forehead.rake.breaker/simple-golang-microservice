package test

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

const (
	countRequests      = 100
	countWorkers       = 5
	sizeWorkersChannel = 200
	testLatency        = "20ms"
)

func TestWorkerPool(t *testing.T) {
	var e IExample

	latency, err := time.ParseDuration(testLatency)
	if err != nil {
		t.Error(err)
	}

	e = &Example{testLatency: latency}
	e = WrapExample(e, sizeWorkersChannel, countWorkers)

	start := time.Now()
	defer func() {
		end := time.Now()
		fmt.Println(end.Sub(start))
	}()

	wg := sync.WaitGroup{}
	for i := 0; i < countRequests; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			e.DoIt()
		}()
	}
	wg.Wait()
}

type RequestMessageExample struct {
	responseChannel chan *ResponseMessageExample
}

type ResponseMessageExample struct {
	Error error
}

type IExample interface {
	DoIt() error
}

type Example struct {
	testLatency time.Duration
}

func (e *Example) DoIt() error {
	time.Sleep(20 * time.Millisecond)
	return nil
}

type WrapperWorkerExample struct {
	example        IExample
	RequestChannel chan *RequestMessageExample
}

func (w *WrapperWorkerExample) DoIt() error {
	responseChannel := make(chan *ResponseMessageExample)
	requestMessageExample := &RequestMessageExample{
		responseChannel: responseChannel,
	}
	w.RequestChannel <- requestMessageExample
	response := <-responseChannel
	return response.Error
}

func WrapExample(example IExample, sizeChan, countWorkers int) IExample {
	requestChannel := make(chan *RequestMessageExample, sizeChan)
	newExample := WrapperWorkerExample{
		example:        example,
		RequestChannel: requestChannel,
	}
	for i := 0; i < countWorkers; i++ {
		go func() {
			newExample.startWorkerExample()
		}()
	}
	return &newExample
}

func (w *WrapperWorkerExample) startWorkerExample() {
	for request := range w.RequestChannel {
		err := w.example.DoIt()
		request.responseChannel <- &ResponseMessageExample{
			Error: err,
		}
	}
}
